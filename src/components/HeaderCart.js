import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Colors from '../consts/Colors'

const HeaderCart = ({ title, onPressBack }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPressBack}>
        <Image
          source={require('../images/ic_back.png')}
          style={{ width: 20, height: 20 }}
          resizeMode='contain'
        />
      </TouchableOpacity>
      <Text style={styles.title}>{title}</Text>
      <View />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    paddingTop: 8,
    backgroundColor: Colors.colorPrimary,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 16
  },
  title: {
    color: 'black',
    marginHorizontal: 8,
    fontSize: 20,
    fontWeight: 'bold'
  }
})
export default HeaderCart