const Colors = {
  colorPrimary: '#ffc107',
  colorSecondary: '#455a64'
}

export default Colors
