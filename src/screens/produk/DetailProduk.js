import React, { Component, Fragment } from 'react'
import { View, Text, SafeAreaView, Image, ScrollView, TouchableOpacity, Alert } from 'react-native'
import Colors from '../../consts/Colors'
import Networks from '../../consts/Networks'
import axios from 'axios'
import icMinus from '../../images/ic_chart_minus.png'
import icPlus from '../../images/ic_chart_plus.png'
import { TextInput } from 'react-native-gesture-handler'
import { openDatabase } from 'react-native-sqlite-storage'
var db = openDatabase({ name: 'UserDatabase.db' })

export default class DetailProduk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      namaBarang: '',
      kode: '',
      price: '',
      diskon: '',
      store: '',
      address: '',
      district: '',
      description: '',
      count: 1,
      storeId: '',
      productId: ''
    }

    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='table_transaction'",
        [],
        function (tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS table_transaction', [])
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_transaction(id INTEGER PRIMARY KEY AUTOINCREMENT,' +
              'store_id VARCHAR(50), product_id VARCHAR(50), name VARCHAR(255), qty INT(10),' +
              'store_name VARCHAR(255), image VARCHAR(255), address VARCHAR(255))',
              []
            )
          }
        }
      )
    })
  }

  componentDidMount() {
    this.getDetail()
  }

  getDetail() {
    axios.get(Networks.BASE_URL + `/products/${this.props.route.params.id}`).
      then(response =>
        this.setState({
          productId: response.data.data.id,
          storeId: response.data.data.store_id,
          image: response.data.data.image,
          namaBarang: response.data.data.name,
          kode: response.data.data.code,
          price: response.data.data.price,
          diskon: response.data.data.voucher === null ? 'Tidak ada diskon' : response.data.data.voucher.name,
          store: response.data.data.store.name,
          address: response.data.data.store.address,
          district: response.data.data.store.district,
          description: response.data.data.store.description
        }))
      .catch(error => console.log('error', error))
  }

  addProduct() {
    Alert.alert('ndsak')
    var that = this
    const { storeId, productId, namaBarang, count, store, image, district } = this.state
    db.transaction(function (tx) {
      tx.executeSql(
        'INSERT INTO table_transaction (store_id, product_id, name, qty, store_name, image, address) VALUES (?,?,?,?,?,?,?)',
        [storeId, productId, namaBarang, count, store, image, district],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            Alert.alert(
              'Success',
              'You are Registered Successfully',
              [
                {
                  text: 'Ok',
                  onPress: () =>
                    that.props.navigation.navigate('Keranjang'),
                },
              ],
              { cancelable: false }
            );
          } else {
            alert('Registration Failed');
          }
        }
      )
    });
  }

  convert(number) {
    var rupiah = "";

    var numberrev = number

      .toString()

      .split("")

      .reverse()

      .join("");

    for (var i = 0; i < numberrev.length; i++)

      if (i % 3 == 0) rupiah += numberrev.substr(i, 3) + ".";

    return (

      "Rp. " +

      rupiah

        .split("", rupiah.length - 1)

        .reverse()

        .join("")

    );
  }


  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.colorPrimary }} />
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, backgroundColor: 'white' }}>
            <ScrollView>
              <View style={{ height: 50, backgroundColor: Colors.colorPrimary, justifyContent: 'center' }}>
                <Text style={{ color: 'black', marginHorizontal: 8, fontSize: 20, fontWeight: 'bold' }}>Detail Produk</Text>
              </View>
              <Image
                source={{ uri: this.state.image }}
                style={{ width: '100%', height: '50%' }}
                resizeMode={'contain'}
              />
              <Text style={{ color: 'black', marginHorizontal: 16, marginVertical: 8 }}>{this.state.namaBarang} - {this.state.kode}</Text>
              <Text style={{ color: 'black', marginHorizontal: 16, marginVertical: 8 }}>{this.convert(this.state.price)} - {this.state.diskon}</Text>
              <Text style={{ color: 'black', marginHorizontal: 16, marginVertical: 8 }}>{this.state.store} - {this.state.address}, {this.state.district}</Text>
              <Text style={{ color: 'black', marginHorizontal: 16, marginVertical: 8 }}>{this.state.description}</Text>
            </ScrollView>
            <View>
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <TouchableOpacity style={{ marginTop: 8 }}>
                  <Image
                    source={icMinus}
                    style={{ height: 30, width: 30, tintColor: Colors.colorPrimary }}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>
                <TextInput
                  underlineColorAndroid={Colors.colorSecondary}
                  style={{ height: 40, width: 60, textAlign: 'center', borderBottomWidth: 1, borderBottomColor: Colors.colorPrimary, marginBottom: 8 }}
                  value={`${this.state.count}`}
                />
                <TouchableOpacity style={{ marginTop: 8 }}>
                  <Image
                    source={icPlus}
                    style={{ height: 30, width: 30, tintColor: Colors.colorPrimary }}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => this.addProduct()}
                style={{ backgroundColor: Colors.colorPrimary, height: 45, alignItems: 'center', justifyContent: 'center' }}
              >
                <Text style={{ color: 'black', fontWeight: 'bold' }}>Tambah ke Keranjang</Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </Fragment>
    )
  }
}
