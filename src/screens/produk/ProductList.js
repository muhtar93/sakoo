import React, { useEffect, useState } from 'react'
import { Text, View, FlatList, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import axios from 'axios'
import Networks from '../../consts/Networks'
import Colors from '../../consts/Colors'
import { convertRupiah } from '../../utils/Convert'

const ProductList = ({ navigation }) => {
  const [list, setList] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    axios.get(Networks.BASE_URL + '/products').
      then(response => {
        setList(response.data.data)
        setIsLoading(false)
      })
      .catch(error => console.log('error', error))
  })

  if (isLoading) {
    return (
      <ActivityIndicator
        size={'large'}
        color={Colors.colorPrimary}
        style={{ marginTop: '50%' }}
      />
    )
  }

  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <FlatList
        data={list}
        style={{ marginTop: 16 }}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => navigation.navigate('Detail', { id: item.id })}>
            <View style={{ marginHorizontal: 16, borderRadius: 8, marginBottom: 16, borderColor: Colors.colorPrimary, borderWidth: 2, flexDirection: 'row' }}>
              <Image
                source={{ uri: item.image }}
                resizeMode={'contain'}
                style={{ width: 100, height: 100 }}
              />
              <View style={{ marginTop: 16, marginHorizontal: 8 }}>
                <Text style={{ color: 'black' }}>{item.name} - {item.code}</Text>
                <Text style={{ color: 'black', marginTop: 8, fontWeight: 'bold', fontSize: 20 }}>{convertRupiah(item.price)}</Text>
                <Text style={{ color: 'black', marginTop: 8 }}>{item.voucher === null ? '' : item.voucher.name}</Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
      />
    </View>
  )
}

export default ProductList