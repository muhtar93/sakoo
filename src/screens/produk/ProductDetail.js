import React, { useEffect, useState, Fragment } from 'react'
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Alert
} from 'react-native'
import axios from 'axios'
import Networks from '../../consts/Networks'
import Colors from '../../consts/Colors'
import { convertRupiah } from '../../utils/Convert'
import icMinus from '../../images/ic_chart_minus.png'
import icPlus from '../../images/ic_chart_plus.png'
import Header from '../../components/Header'
import { openDatabase } from 'react-native-sqlite-storage'

const db = openDatabase({ name: 'UserDatabase.db' })

const addProduct = (storeId, productId, namaBarang, count, store, image, district, { navigation }) => {
  Alert.alert('ndsak')
  var that = this
  db.transaction(function (tx) {
    tx.executeSql(
      'INSERT INTO table_transaction (store_id, product_id, name, qty, store_name, image, address) VALUES (?,?,?,?,?,?,?)',
      [storeId, productId, namaBarang, count, store, image, district],
      (tx, results) => {
        console.log('Results', results.rowsAffected);
        if (results.rowsAffected > 0) {
          Alert.alert(
            'Success',
            'You are Registered Successfully',
            [
              {
                text: 'Ok',
                onPress: () =>
                  navigation.navigate('Keranjang'),
              },
            ],
            { cancelable: false }
          );
        } else {
          Alert.alert('Alert', 'Add to cart Failed')
        }
      }
    )
  })
}

const ProductDetail = ({ route, navigation }) => {
  const [image, setImage] = useState(null)
  const [productName, setProductName] = useState('')
  const [code, setCode] = useState('')
  const [price, setPrice] = useState('')
  const [discount, setDiscount] = useState('')
  const [storeName, setStoreName] = useState('')
  const [address, setAddress] = useState('')
  const [district, setDistrict] = useState('')
  const [description, setDescription] = useState('')
  const [qty, setQty] = useState(1)
  const [storeId, setStoreId] = useState('')
  const [productId, setProductId] = useState('')

  useEffect(() => {
    axios.get(Networks.BASE_URL + `/products/${route.params.id}`).
      then(response => {
        setProductId(response.data.data.id)
        setStoreId(response.data.data.store_id)
        setImage(response.data.data.image)
        setProductName(response.data.data.name)
        setCode(response.data.data.code)
        setPrice(response.data.data.price)
        setDiscount(response.data.data.voucher === null ? 'Tidak ada diskon' : response.data.data.voucher.name)
        setStoreName(response.data.data.store.name)
        setAddress(response.data.data.store.address)
        setDistrict(response.data.data.store.district)
        setDescription(response.data.data.store.description)
      })
      .catch(error => console.log('error', error))
  })

  useEffect(() => {
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='table_transaction'",
        [],
        function (tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS table_transaction', [])
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_transaction(id INTEGER PRIMARY KEY AUTOINCREMENT,' +
              'store_id VARCHAR(50), product_id VARCHAR(50), name VARCHAR(255), qty INT(10),' +
              'store_name VARCHAR(255), image VARCHAR(255), address VARCHAR(255))',
              []
            )
          }
        }
      )
    })
  })

  return (
    <Fragment>
      <SafeAreaView style={styles.areaPrimary} />
      <SafeAreaView style={styles.areaSecondary}>
        <View style={[styles.areaSecondary, { backgroundColor: 'white' }]}>
          <ScrollView>
            <Header
              title='Detail Produk'
              onPressBack={() => navigation.goBack()}
              onPressCart={() => navigation.navigate('Keranjang')}
            />
            <Image
              source={{ uri: image }}
              style={styles.image}
              resizeMode={'contain'}
            />
            <Text style={styles.text}>{productName} - {code}</Text>
            <Text style={styles.text}>{convertRupiah(price)} - {discount}</Text>
            <Text style={styles.text}>{storeName} - {address}, {district}</Text>
            <Text style={styles.text}>{description}</Text>
          </ScrollView>
          <View>
            <View style={styles.containerCart}>
              <TouchableOpacity style={styles.btn}>
                <Image
                  source={icMinus}
                  style={styles.btnQty}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
              <TextInput
                underlineColorAndroid={Colors.colorSecondary}
                style={styles.input}
                value={`${qty}`}
              />
              <TouchableOpacity style={styles.btn}>
                <Image
                  source={icPlus}
                  style={styles.btnQty}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => addProduct(storeId, productId, productName, qty, storeName, image, district, { navigation })}
              style={styles.btnCart}>
              <Text style={styles.textCart}>Tambah ke Keranjang</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    </Fragment>
  )
}

const styles = StyleSheet.create({
  areaPrimary: {
    flex: 0,
    backgroundColor: Colors.colorPrimary
  },
  areaSecondary: {
    flex: 1
  },
  image: {
    width: '100%', height: '100%'
  },
  text: {
    color: 'black',
    marginHorizontal: 16,
    marginVertical: 8
  },
  containerCart: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  btn: {
    marginTop: 8
  },
  btnQty: {
    height: 30,
    width: 30,
    tintColor: Colors.colorPrimary
  },
  input: {
    height: 40, width: 60,
    textAlign: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.colorPrimary,
    marginBottom: 8
  },
  btnCart: {
    backgroundColor: Colors.colorPrimary,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textCart: {
    color: 'black',
    fontWeight: 'bold'
  }
})

export default ProductDetail