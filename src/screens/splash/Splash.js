import React, { Component } from 'react'
import { View, Image, Animated, Text } from 'react-native'

export default class Splash extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fadeAnim: new Animated.Value(0)
    }
  }

  componentDidMount() {
    Animated.timing(
      this.state.fadeAnim,
      {
        useNativeDriver: true,
        toValue: 1,
        duration: 3000
      }
    ).start(async () => {
      this.props.navigation.reset({
        index: 0,
        routes: [{ name: 'Tab' }]
      })
    })
  }

  render() {
    return (
      <Animated.View style={{ flex: 1, opacity: this.state.fadeAnim, ...this.props.style }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            source={require('../../images/sakoo_splash.png')}
            style={{ width: '80%', height: '50%', marginHorizontal: 16 }}
            resizeMode={'contain'}
          />
          <Text style={{ color: 'black', fontSize: 32, fontWeight: 'bold' }}>Sakoo</Text>
        </View>
      </Animated.View>
    )
  }
}
