import React, { Component, Fragment } from 'react'
import { View, Text, SafeAreaView, FlatList } from 'react-native'
import Colors from '../../consts/Colors'
import { openDatabase } from 'react-native-sqlite-storage'
import HeaderCart from '../../components/HeaderCart'
var db = openDatabase({ name: 'UserDatabase.db' })

export default class Keranjang extends Component {
  constructor(props) {
    super(props)
    this.state = {
      FlatListItems: [],
    };
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM table_transaction', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        this.setState({
          FlatListItems: temp,
        })
      })
    })
  }

  render() {
    console.log('list transaksi', this.state.FlatListItems)
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.colorPrimary }} />
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, backgroundColor: 'white' }}>
            <HeaderCart title='Keranjang' onPressBack={() => this.props.navigation.goBack()} />
            <FlatList
              data={this.state.FlatListItems}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <View key={item.id} style={{ backgroundColor: 'white', padding: 20 }}>
                  <Text>{item.store_name}</Text>
                  <Text>{item.address}</Text>
                  <Text>{item.name}</Text>
                </View>
              )}
            />
          </View>
        </SafeAreaView>
      </Fragment>
    )
  }
}
