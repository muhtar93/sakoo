import React, { Component, Fragment } from 'react'
import { View, Text, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
import Colors from '../../consts/Colors'
import Networks from '../../consts/Networks'
import axios from 'axios'

export default class ListTransaksi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transactions: []
    }
  }

  getProduct() {
    axios.post(Networks.BASE_URL + '/transactions',
      {
        "user_id": "muhtar081290",
        "address": "Jl. Tanimulya Raya No. 9",
        "cart": [
          {
            "store_id": "d2f95bbe-159b-4447-9e07-33925e04c5e9",
            "expedition_id": "c7999dce-19b7-4bfd-84cc-d2f0845a3d86",
            "products": [
              {
                "product_id": "b648ff73-468f-45cb-bbbc-fd3659e4b8ac",
                "stock": 1
              }
            ]
          }
        ]
      }
    ).
      then(response => console.log('response', response.data))
      .catch(error => console.log('error', error))
  }

  async getTransaction() {
    await axios.get(Networks.BASE_URL + '/transactions/muhtar081290').
      then(response => this.setState({ transactions: response.data.data }))
      .catch(error => console.log('error', error))
  }

  componentDidMount() {
    this.getTransaction()
    this.convertDate()
  }

  convertDate(createdAt) {
    const date = new Date(createdAt)
    const dateTransaction = date.toLocaleDateString()
    return dateTransaction
  }

  render() {
    console.log('list transaksi', this.state.transactions)
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.colorPrimary }} />
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, backgroundColor: 'white' }}>
            <FlatList
              data={this.state.transactions}
              style={{ marginTop: 16 }}
              renderItem={({ item }) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailTransaksi', {
                  invoiceNumber: item.invoice_number,
                  createdAt: this.convertDate(item.created_at),
                  data: item.data,
                  courier: item.data.expedition,
                  totalPrice: item.total_price
                })}>
                  <View style={{ marginHorizontal: 16, borderRadius: 8, marginBottom: 16, borderColor: Colors.colorPrimary, borderWidth: 2, paddingHorizontal: 8, paddingVertical: 8 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 8 }}>
                      <Text>{item.invoice_number}</Text>
                      <Text>{this.convertDate(item.created_at)}</Text>
                    </View>
                    <Text>{item.total_price}</Text>
                  </View>
                </TouchableOpacity>
              )}
              keyExtractor={item => item.id}
            />
          </View>
        </SafeAreaView>
      </Fragment>
    )
  }
}
