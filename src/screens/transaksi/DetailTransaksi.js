import React, { Component, Fragment } from 'react'
import { View, Text, SafeAreaView, FlatList, Image } from 'react-native'
import Colors from '../../consts/Colors'

export default class DetailTransaksi extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  renderProduct(products) {
    console.log('products', products)
    return products.map(item =>
      <View style={{ flexDirection: 'row' }}>
        <Image
          source={{ uri: item.product.image }}
          resizeMode={'contain'}
          style={{ width: 100, height: 100 }}
        />
        <View style={{ marginLeft: 16, marginTop: 8 }}>
          <Text>{item.product.name}</Text>
          <Text>{item.product.price}</Text>
          <Text>{item.stock} pcs</Text>
        </View>
      </View>
    )
  }

  render() {
    const { invoiceNumber, createdAt, data, courier, totalPrice } = this.props.route.params
    // console.log('data', data)
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.colorPrimary }} />
        <SafeAreaView style={{ flex: 1 }}>
          <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={{ height: 50, backgroundColor: Colors.colorPrimary, justifyContent: 'center' }}>
              <Text style={{ color: 'black', marginHorizontal: 8, fontSize: 20, fontWeight: 'bold' }}>Detail Transaksi</Text>
            </View>
            <View style={{ padding: 8 }}>
              <Text>Invoice Number - {invoiceNumber}</Text>
              <Text style={{ marginTop: 4 }}>{createdAt}</Text>
            </View>
            <FlatList
              data={data}
              renderItem={({ item }) => (
                <View>
                  {this.renderProduct(item.products)}
                  <Text>{item.expedition.name}</Text>
                  <Text>{item.expedition.price}</Text>
                  <Text>{totalPrice}</Text>
                </View>
              )}
              keyExtractor={item => item.store_id}
            />
          </View>
        </SafeAreaView>
      </Fragment>
    )
  }
}
