import 'react-native-gesture-handler'
import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import Splash from '../screens/splash/Splash'
import Tab from '../navigations/Tab'
import DetailProduk from '../screens/produk/DetailProduk'
import DetailTransaksi from '../screens/transaksi/DetailTransaksi'
import Keranjang from '../screens/keranjang/Keranjang'
import ProductDetail from '../screens/produk/ProductDetail'

const Stack = createStackNavigator()

const Main = () => {
  const options = {
    gestureEnabled: true,
    ...TransitionPresets.SlideFromRightIOS
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Splash'
        screenOptions={{
          headerShown: false,
          cardStyle: {
            backgroundColor: 'white'
          }
        }}>
        <Stack.Screen name='Splash' component={Splash} options={options} />
        <Stack.Screen name='Tab' component={Tab} options={options} />
        <Stack.Screen name='Detail' component={ProductDetail} options={options} />
        <Stack.Screen name='Keranjang' component={Keranjang} options={options} />
        <Stack.Screen name='DetailTransaksi' component={DetailTransaksi} options={options} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Main