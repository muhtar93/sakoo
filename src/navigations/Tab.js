import 'react-native-gesture-handler'
import * as React from 'react'
import { View, Text, SafeAreaView } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import ListTransaksi from '../screens/transaksi/ListTransaksi'
import Colors from '../consts/Colors'
import ProductList from '../screens/produk/ProductList'

const TopTab = createMaterialTopTabNavigator()

const TabNav = () => {
  return (
    <TopTab.Navigator
      initialRouteName='ListProduk'
      tabBarOptions={{
        activeTintColor: Colors.colorPrimary,
        inactiveTintColor: 'white',
        style: {
          backgroundColor: Colors.colorSecondary,
        },
        labelStyle: {
          textAlign: 'center',
          textTransform: 'none'
        },
        indicatorStyle: {
          borderBottomColor: Colors.colorPrimary,
          borderBottomWidth: 2,
        },
      }}>
      <TopTab.Screen
        name='ListProduk'
        component={ProductList}
        options={{
          tabBarLabel: 'Produk'
        }} />
      <TopTab.Screen
        name='Transaksi'
        component={ListTransaksi}
        options={{
          tabBarLabel: 'Transaksi',

        }} />
    </TopTab.Navigator>
  )
}


class Tab extends React.Component {
  render() {
    return (
      <React.Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: Colors.colorPrimary }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
          <View style={{ flex: 1 }}>
            <View style={{ backgroundColor: Colors.colorPrimary, height: 50, justifyContent: 'center' }}>
              <Text style={{ color: 'black', marginHorizontal: 8, fontSize: 20, fontWeight: 'bold' }}>Sakoo</Text>
            </View>
            <TabNav />
          </View>
        </SafeAreaView>
      </React.Fragment>
    )
  }
}

export default Tab