import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import BookList from '../screens/BookList'
import BookmarkList from '../screens/BookmarkList'
import { Image } from 'react-native'

const Tab = createBottomTabNavigator()

const tabBarOptions = {
  showLabel: false,
  inactiveTintColor: 'yellow',
  activeTintColor: '#FFF',
  style: {
    height: '10%',
    backgroundColor: '#1E1B26'
  }
}

const screenOptions = (route, color) => {
  let iconName

  switch (route.name) {
    case 'BookList':
      iconName = require('../images/ic_chart_minus.png')
      break;
    case 'BookmarkList':
      iconName = require('../images/ic_chart_minus.png')
    default:
      break;
  }

  return <Image source={iconName} style={{ width: 24, height: 24 }} />
}

const RootNavigator = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName='BookList'
        tabBarOptions={{
          showLabel: false,
          activeTintColor: 'red',
          activeBackgroundColor: 'red',
          inactiveBackgroundColor: 'grey',
          style: {
            backgroundColor: '#1E1B26',
            height: '10%'
          }
        }}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ color }) => screenOptions(route, color)
        })}
      >
        <Tab.Screen name='BookList' component={BookList} />
        <Tab.Screen name='BookmarkList' component={BookmarkList} />
      </Tab.Navigator>
    </NavigationContainer>
  )
}

export default RootNavigator